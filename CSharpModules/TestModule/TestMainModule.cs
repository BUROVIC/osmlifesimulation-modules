﻿using Microsoft.Xna.Framework;
using OSMGlobalLibrary.SuperModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WorkWithPeopleModule
{
    public class TestMainModule : OSMModule
    {
        const string style = @"
		    image: new ol.style.Circle({
		    opacity: 1.0,
		    scale: 1.0,
		    radius: 3,
		    fill: new ol.style.Fill({
		      color: 'rgba(21, 21, 155, 0.8)'
		    }),
		    stroke: new ol.style.Stroke({
		      color: 'rgba(5, 5, 53, 1)',
		      width: 1
		    }),

	      })
        ";

        public TestMainModule(object data, Dictionary<string, OSMModule> modules) 
            : base(data, modules)
        {
        }

        public override void Update(long elapsedMilliseconds)
        {
            Console.WriteLine("Curent time: " + elapsedMilliseconds);
            Console.WriteLine("People module contains {0} characters", GetAnotherModuleAsDynamic("People").Characters.Count);
            anotherModules["People"].Execute("TestMethod");
        }

        public override List<(List<Point>, string)> DrawableData()
        {
            return new List<(List<Point>, string)>() {
                (new List<Point>() { new Point(462256, 5406052) }, style)
            };
        }
    }
}
