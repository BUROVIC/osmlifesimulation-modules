﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PeopleModule
{
    public class Character
    {
        List<Point> path;
        public Point Point;

        //no deseleration if 0
        int Deceleration = 10;

        int speed = 1;

        Point nextPoint;

        public Character(List<Point> path, int deceleration)
        {
            this.path = path;
            Point = path[0];
            nextPoint = path.Count != 1 ? path[1] : path[0];

            Deceleration = deceleration;
        }

        public bool WillBeUpdated()
        {
            return ((Random)Assembly.GetEntryAssembly().GetTypes().Where(x => x.IsClass).ToList().Find(x => x.Name == "Constants").GetProperty("Rnd").GetValue(null)).Next(Deceleration) == 0;
        }
        public void Update(out bool remove)
        {
            if (WillBeUpdated())
            {
                if (nextPoint != Point.Zero)
                {
                    if (move())
                    {
                        nextPoint = getNextNode();
                    }
                    remove = false;
                }
                else
                {
                    remove = true;
                }
            }
            else
            {
                remove = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>next node reached</returns>
        private bool move()
        {
            if (Point == nextPoint)
                return true;

            if (Point.X != nextPoint.X)
            {
                Point.X += Point.X < nextPoint.X ? speed : -speed;
            }
            if (Point.Y != nextPoint.Y)
            {
                Point.Y += Point.Y < nextPoint.Y ? speed : -speed;
            }

            return false;
        }

        private Point getNextNode()
        {
            int indexNext = path.IndexOf(nextPoint) + 1;
            return indexNext != path.Count ? path[indexNext] : Point.Zero;
        }
    }
}
