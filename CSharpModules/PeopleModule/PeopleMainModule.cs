﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using OSMGlobalLibrary.SuperModule;

namespace PeopleModule
{
    public class PeopleMainModule : OSMModule
    {
        public List<Character> Characters = new List<Character>();

        long lastCharacterAdded = 0;

        Random rnd;

        List<Point> entrances;

        const string characterStyle = @"
		    image: new ol.style.Circle({
		    opacity: 1.0,
		    scale: 1.0,
		    radius: 3,
		    fill: new ol.style.Fill({
		      color: 'rgba(155, 21, 21, 0.8)'
		    }),
		    stroke: new ol.style.Stroke({
		      color: 'rgba(53, 5, 5, 1)',
		      width: 1
		    }),

	      })
        ";

        public PeopleMainModule(object data, Dictionary<string, OSMModule> modules) : base(data, modules)
        {
            entrances = (List<Point>)data.GetType().GetProperty("Entrances").GetValue(data);
            this.rnd = (Random)types.Find(x => x.Name == "Constants").GetProperty("Rnd").GetValue(null);
        }

        public override void Update(long elapsedMilliseconds)
        {
            var delay = 1000;
            if (elapsedMilliseconds - lastCharacterAdded > delay)
            {
                lastCharacterAdded = elapsedMilliseconds;

                var sourceNode = entrances[rnd.Next(entrances.Count - 1)];
                var targetNode = entrances.Where(n => n != sourceNode).ToList()[rnd.Next(entrances.Count - 1)];
                var path = ((Task<List<Point>>)types.Find(x => x.Name == "PathFinding").GetMethod("GetPath").Invoke(null, new[] { (object)sourceNode, targetNode })).Result;
                Characters.Add(new Character(path, rnd.Next(3, 30)));
            }

            try
            {
                foreach (var character in Characters.ToList())
                {
                    bool remove;
                    character.Update(out remove);
                    if (remove)
                    {
                        Characters.Remove(character);
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        public override List<(List<Point>, string)> DrawableData()
        {
            return new List<(List<Point>, string)>() {
                (Characters.Select(c => c.Point).ToList(), characterStyle)
            };
        }
    }
}
