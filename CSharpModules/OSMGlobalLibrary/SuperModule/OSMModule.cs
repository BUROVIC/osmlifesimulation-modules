﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace OSMGlobalLibrary.SuperModule
{
    public abstract class OSMModule
    {
        protected Dictionary<string, OSMModule> anotherModules { get; }

        protected List<Type> types;

        public OSMModule(object data, Dictionary<string, OSMModule> modules)
        {
            types = Assembly.GetEntryAssembly().GetTypes().Where(x => x.IsClass).ToList();
            anotherModules = modules;
        }

        public abstract void Update(long elapsedMilliseconds);

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Возвращает лист кортежей <точки, стиль></returns>
        public virtual List<(List<Point>, string)> DrawableData()
        {
            return new List<(List<Point>, string)>();
        }

        public object Execute(string methodName, object[] patemeters = null)
        {
            try
            {
                return GetType().GetMethod(methodName).Invoke(this, patemeters);
            }
            catch (Exception)
            {
                Log(String.Format("Some error with {0} method call", methodName));
                return null;
            }
            
        }

        protected void Log(string msg)
        {
            types.Find(x => x.Name == "Constants").GetMethod("Log").Invoke(null, new object[]{ msg, this });
        }

        public object GetProperty(string name)
        {
            return GetType().GetProperty(name).GetValue(this);
        }

        public dynamic GetAnotherModuleAsDynamic(string name)
        {
            return anotherModules[name];
        }
    }
}
